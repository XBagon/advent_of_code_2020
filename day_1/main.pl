part1() :- open("input.txt", read, Stream), list_of_lines(Stream, Lines), nth0(I0, Lines, A), nth0(I1, Lines, B), I0 < I1, 2020 is A + B, R is A * B, writeln(R).
part2() :- open("input.txt", read, Stream), list_of_lines(Stream, Lines), nth0(I0, Lines, A), nth0(I1, Lines, B), nth0(I2, Lines, C), I0 < I1, I1 < I2, 2020 is A + B + C, R is A * B * C, writeln(R).

list_of_lines(Stream, [Number|List]) :- read_line_to_string(Stream, String), (String \= end_of_file), number_string(Number, String), list_of_lines(Stream, List).
list_of_lines(_, []).

